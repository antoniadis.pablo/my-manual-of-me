# User Manual

A User Manual for working with me.

## Conditions I like to work in 
I like to work in quiet places. But I also appreciate when there is some background discussions even thought I'm not participing in it.

## The time/hours I like to work
I'm more of a night person. In France I used to work very late. Happend to me to finish work at 3am - 5am. But since I'm here in Japan I just follow the same offices hours of everyone. 

## The best ways to communicate with me
Just a slack message is fine for me. If it's urgent I'm more reactive on Whatsapp or Line.

## The ways I like to receive feedback
If it's a design UX/UI related feedback I like to talk about it in front of the project so it's easier for me to see clearly what is the request and on which particular element. For other style of projects I prefer written reviews since I can come back to it later.  

## Things I need
Since I began working with 2 monitors I found difficult to come back to only one monitor even in split screen. Usualy it's one for the code and the second for the output of the code.

## Things I struggle with
If I don’t understand what is the final objective of the project I found really dificult to start working in it. I like to have the global view of the project first and then go work on small details to get to that final objective. 

## Things I love
I like bouldering, hiking, fencing and eat tons of food. I also like to draw things I like and display it on a post-it note on my desk. 

## Other things to know about me
When I have new year's resolutions I manage to accomplish them before the end of the year. Last year was loosing 10kg and coming to Japan. This year it's find traces of my grandfather's passage in Japan (and learn japanese but I don't think that 1 year is anough).  
